# README #

Moodle Latex Parser requires following libraries: tkinter, TexSoup, xml. Install missing libraries using pip.

### What is this repository for? ###

* Parser takes Latex document with specific structure and transforms it to the xml Moodle file for quiz question import

### How do I get set up? ###

* Summary of set up
* Configuration
* Dependencies
* Database configuration
* How to run tests
* Deployment instructions

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Lukas Pohl
* lukas.pohl@ceitec.vutbr.cz