from tkinter import *
from tkinter import messagebox
from tkinter.filedialog import * 
from xml.etree import ElementTree
from xml.etree.ElementTree import Element, SubElement, Comment
from xml.dom import minidom
from pdf2image import convert_from_path
from pdf2image.exceptions import (
    PDFInfoNotInstalledError,
    PDFPageCountError,
    PDFSyntaxError
)
import base64
import tempfile
import pickle
import os
import TexSoup
#import webview # TODO preview of html formated text

# TODO:
# - check for OS specific hardcoded paths, use platform independent normalized paths
# - remove all hardcoded filenames, foldernames
##########################################################################################

default_conf = {"maxPointsForAnswer":"2.0",
                "categoryText":"<p>Test category title with HTML tags<br></p>",
                "categoryPath":"$course$/top/Category path in course",
                "questionName":"Question name ",
                "correctFeedbackText":"Correct answer reaction.",
                "incorrectFeedbackText":"Incorrect answer reaction.",
                "partiallyCorrectFeedbackText":"Partially correct answer reaction.",
                "shuffleAnswers":TRUE,
                "parseImg":FALSE}
conf_keys    = {"maxPointsForAnswer",
                "categoryText",
                "categoryPath",
                "questionName",
                "correctFeedbackText",
                "incorrectFeedbackText",
                "partiallyCorrectFeedbackText",
                "shuffleAnswers",
                "parseImg"}                

conf_global = default_conf

#soup = ""


##########################################################################################

class Assignment:
    def __init__(self):
        self.question = []
        self.imgPath = []
        self.imgData = {}
        self.math = []
        self.answer = []
        self.note = []

def _serialize_xml(write, elem, qnames, namespaces,short_empty_elements, **kwargs):
    # ElementTree serialize with CDATA support
    if elem.tag == '![CDATA[':
        write("<{}{}]]>".format(elem.tag, elem.text))
        if elem.tail:
            write(_escape_cdata(elem.tail))
    else:
        return ElementTree._original_serialize_xml(write, elem, qnames, namespaces,short_empty_elements, **kwargs)               

def prettify(elem):
    # Return a pretty-printed XML string for the Element.
    rough_string = ElementTree.tostring(elem, 'utf-8')
    reparsed = minidom.parseString(rough_string)
    return reparsed.toprettyxml(indent="  ")

def CDATA(text=None):
    element = ElementTree.Element('![CDATA[')
    element.text = text
    return element

def replaceMathEnv(text):
    while (start := text.find('$')) >= 0:
        end = text.find('$',start + 1)
        if end<0:
            end = len(text)
        # replace all $ delimiters with \( \)
        text = text[:start] + "\(" + text[start+1:end] + "\)" + text[end+1:]
    return text

def parseItems(soup):
    """
    TODO: search for \graphicspath and add it to the path of image
    """
    if soup is not None:
        items = soup.find_all('item')
    examList = []
    for item in items:
        if "document" in item.parent.parent.name:
            examList.append(Assignment()) 
            for idx, content in enumerate(item.contents):
                if isinstance(content,str):
                    # content is string
                    if not content.strip().startswith("%"):
                        # ignore commented text
                        examList[-1].question.append([idx, content.strip() + " "])         
                else:
                    # content is latex expression
                    if "align" in content.name:
                        # content is align named environment, each align begins at new line
                        examList[-1].math.append([idx, "<br>\\(" + str(content) + "\\)<br> "])
                    elif "enumerate" in content.name:
                        for enumItem in content.children:
                            # expecting just single content children, can fail
                            # answer items are not using node order (idx) they use true or false to denote correct answer
                            strippedStr = replaceMathEnv(str(enumItem).replace("\\item","").strip())
                            strippedStr = strippedStr.replace("<CORR>","")
                            examList[-1].answer.append(["<CORR>" in str(enumItem) ,strippedStr])
                    elif "input" in content.name:
                        imgFile = os.path.split(content.contents[0])[1]
                        name = os.path.splitext(imgFile)[0]
                        examList[-1].imgPath.append([idx, content.contents[0]])
                        examList[-1].imgData[name] = ""
                    elif "includegraphics" in content.name:
                        imgFile = os.path.split(content.contents[0])[1]
                        name = os.path.splitext(imgFile)[0]
                        examList[-1].imgPath.append([idx, content.contents[0]])
                        examList[-1].imgData[name] = ""
                    elif "$" in content.name:
                        # inline math expression is expected only in plain text
                        # TODO: extend to enumerate answers
                        examList[-1].question.append([idx, "\(" + str(content)[1:-1] + "\) "])
    return examList

def constructXml(examList, conf):
    answTextHtmlStart = """<table style="border-width: 1px; background-color:white; border-style: solid; width: 500px;"><thead><tr><th scope="col">"""
    answTextHtmlEnd = """<br></th></tr></thead><tbody></tbody></table>"""
    imgStart = """<br></p><p><img src="@@PLUGINFILE@@/"""
    imgEnd = """" alt="" role="presentation" class="img-responsive atto_image_button_text-bottom" width="655" height="265"><br></p>"""

    # build category for moodle xml
    xmlTop = Element('quiz')
    xmlTop.append(Comment(" question: 0 "))
    xmlQuestion = SubElement(xmlTop,"question",attrib={"type":"category"})
    xmlCategory = SubElement(xmlQuestion,"category")
    xmlText = SubElement(xmlCategory,"text").text = conf["categoryPath"]
    xmlInfo = SubElement(xmlQuestion,"info",attrib={"format":"html"})
    xmlText = SubElement(xmlInfo,"text")
    xmlText.append(CDATA(conf["categoryText"]))
    xmlInfo = SubElement(xmlQuestion,"idnumber")

    # build individual questions
    for num, examQuestion in enumerate(examList):
        plainTextExamItem = {}
        xmlTop.append(Comment(" question: " + str(num+1) + " "))
        xmlQuestion = SubElement(xmlTop,"question",attrib={"type":"multichoice"})
        xmlName = SubElement(xmlQuestion,"name")
        xmlText = SubElement(xmlName,"text")
        xmlText.text = conf["questionName"] + str(num+1).zfill(2) 
        xmlQuestionText = SubElement(xmlQuestion,"questiontext",attrib={"format":"html"})
        xmlText = SubElement(xmlQuestionText,"text")

        # compose question in a form of dictionary composed of ordered parts (plain text, symbols, equations, etc.)
        # dictionary can be then sorted by order, works even if some indexes are missing
        for q in examQuestion.question:
            plainTextExamItem[q[0]]=q[1]
        for m in examQuestion.math:
            plainTextExamItem[m[0]]=m[1]
        for m in examQuestion.imgPath:
            imgFile = os.path.split(m[1])[1]
            imageName = os.path.splitext(imgFile)[0]
            # xml embedded images - create html tag
            if conf["parseImg"]:
                plainTextExamItem[m[0]] = imgStart + imageName + ".PNG" + imgEnd
            # manually inserted images - create placeholder    
            else:
                plainTextExamItem[m[0]] = "PLACE IMAGE:" + imageName + " HERE"
        # join sorted dictionary to a single string with moodle html formating
        xmlText.append(CDATA('<p>' + ''.join(str(plainTextExamItem[i]) for i in sorted(plainTextExamItem)) + '</p>'))
        if conf["parseImg"]:    
            for image in examQuestion.imgPath:
                imgFile = os.path.split(image[1])[1]
                imageName = os.path.splitext(imgFile)[0]
                xmlImg = SubElement(xmlQuestionText,"file",attrib={ "name":imageName+".PNG", "path":"/", "encoding":"base64" })
                xmlImg.text = examQuestion.imgData[imageName]                
            
        xmlGenFeed = SubElement(xmlQuestion,"generalfeedback",attrib={"format":"html"})
        xmlText = SubElement(xmlGenFeed,"text")
        xmlDefaultGrade = SubElement(xmlQuestion,"defaultgrade")
        xmlDefaultGrade.text = conf["maxPointsForAnswer"]
        xmlPenalty = SubElement(xmlQuestion,"penalty")
        xmlPenalty.text = "0.333333"
        xmlHidden = SubElement(xmlQuestion,"hidden")
        xmlHidden.text = "0"
        SubElement(xmlQuestion,"idnumber")   
        xmlSingle = SubElement(xmlQuestion,"single")
        ncorr = 0
        for i in range(len(examQuestion.answer)):
            if examQuestion.answer[i][0]:
                ncorr+=1
        xmlSingle.text = "false" if ncorr>1 else "true"
        xmlShuffleanswers = SubElement(xmlQuestion,"shuffleanswers")
        xmlShuffleanswers.text = "true" if conf["shuffleAnswers"] else "false"
        xmlAnswernumbering = SubElement(xmlQuestion,"answernumbering")
        xmlAnswernumbering.text = "abc"
        xmlCorrectfeedback = SubElement(xmlQuestion,"correctfeedback",attrib={"format":"html"})
        xmlText = SubElement(xmlCorrectfeedback,"text")
        xmlText.text = conf["correctFeedbackText"]
        xmlPartCorrectfeedback = SubElement(xmlQuestion,"partiallycorrectfeedback",attrib={"format":"html"})
        xmlText = SubElement(xmlPartCorrectfeedback,"text")
        xmlText.text = conf["partiallyCorrectFeedbackText"]
        xmlIncorrectfeedback = SubElement(xmlQuestion,"incorrectfeedback",attrib={"format":"html"})
        xmlText = SubElement(xmlIncorrectfeedback,"text")
        xmlText.text = conf["incorrectFeedbackText"]
        SubElement(xmlQuestion,"shownumcorrect")
        for a in examQuestion.answer:
            xmlAnswer = SubElement(xmlQuestion,"answer",attrib={"fraction":str(100/ncorr) if a[0] else "0","format":"html"})
            xmlText = SubElement(xmlAnswer,"text")
            xmlText.append(CDATA(answTextHtmlStart + a[1] + answTextHtmlEnd))
            xmlFeedback = SubElement(xmlAnswer,"feedback")
            xmlText = SubElement(xmlFeedback,"text")
    return xmlTop
    
class Application(Frame):
    """
    GUI main class.
    """
    def __init__(self, master):
        self.soup = TexSoup.TexSoup("")
        self.categoryText = StringVar()
        self.categoryPath = StringVar()
        self.questionName = StringVar()
        self.correctFeedbackText = StringVar()
        self.incorrectFeedbackText = StringVar()
        self.partiallyCorrectFeedbackText = StringVar()
        self.maxPointsForAnswer = StringVar()
        self.shuffleAnswers = BooleanVar(FALSE) 
        self.parseImg = BooleanVar(FALSE)
        self.pathToMikTex = StringVar()
        self.pathToMikTex.set("C:\\MiKTeX\\miktex\\bin\\x64")
        self.useDocker = BooleanVar(FALSE)
        # Initialize Frame
        super(Application, self).__init__(master)
        self.grid(padx=10, pady=5)
        self.create_widgets()

    def readEntries(self):
        conf = {}
        for key in conf_keys:
            conf[key] = getattr(self, key).get() 
        return conf

    def writeEntries(self, conf):
        for key in conf_keys:
            getattr(self, key).set(conf[key])

    def compileTex(self, path):
        texHead = "\\documentclass{standalone}\\usepackage{pst-sigsys}\\begin{document}"
        texFoot = "\\end{document}"
        noExtPath = os.path.splitext(path)[0]
        imgDir = os.path.split(path)[0]
        with open(path,"r") as file:
            body = file.read()
            text = texHead + body + texFoot
            with open(noExtPath + "TMP.tex","w") as file:
                file.write(text)
        if self.useDocker.get():
            print("TBD docker support")
        else:    
            if os.path.isfile(self.pathToMikTex.get() + os.sep + "xelatex" + ".exe" if os.name=='nt' else ""): 
                commandStr = self.pathToMikTex.get() + \
                                os.sep + \
                                "xelatex" + \
                                " -interaction=batchmode " + \
                                "-output-directory=" + imgDir + \
                                " " + noExtPath + "TMP.tex"
                os.system(commandStr)
                os.remove(noExtPath + "TMP.aux")
                os.remove(noExtPath + "TMP.log")
                os.remove(noExtPath + "TMP.tex")                  
            else:
                messagebox.showerror("Error", "Incorrect MiKTeX path")
        if os.path.isfile(noExtPath + "TMP.pdf"):                   
            with tempfile.TemporaryDirectory() as path:
                # TODO: search for poppler in cwd instead of using hardcoded path
                hardcodedPopplerPath = "\\poppler\\bin\\"
                images = convert_from_path(noExtPath + "TMP.pdf", 300, poppler_path=os.getcwd()+hardcodedPopplerPath, output_folder=path)
                images[0].save(noExtPath + ".png", 'PNG')
                os.remove(noExtPath + "TMP.pdf") 

    def openFile(self, options):
        if options=="tex":
            types = [("Latex Files","*.tex")]
        if options=="cfg":
            types = [("Configuration Files","*.cfg")]
        filename = askopenfilename(filetypes = types)
        if filename is not None and options=="tex":
            with open(filename, "r",encoding='utf-8') as file:
                self.soup = TexSoup.TexSoup(file)
                self.statusBar['text'] = "Successful Latex import"
                self.statusBar['foreground'] = "green"
        if filename is not None and options=="cfg":
            try:
                with open(filename, "rb") as file:
                    self.writeEntries(pickle.load(file))
                    self.statusBar['text'] = "Successful config import"
                    self.statusBar['foreground'] = "green"
            except pickle.UnpicklingError:
                self.statusBar['text'] = "Unsuccessful config import"
                self.statusBar['foreground'] = "red"
    def saveFile(self, options):
        if options=="xml":
            types = [('XML Files', '*.xml')]
        if options=="cfg":
            types = [("Configuration Files","*.cfg")]
        fileName = asksaveasfilename(filetypes=types,defaultextension="*.*")
        if fileName is not None and options=="cfg":
            file = open(fileName, 'wb')
            pickle.dump(self.readEntries(), file)
            file.close()
            self.statusBar['text'] = "Configuration saved"
            self.statusBar['foreground'] = "green"
        if fileName is not None and options=="xml":
            file = open(fileName, 'w', encoding='utf-8')
            examList = parseItems(self.soup)
            if self.parseImg.get():
                # store base64 coded images by their names, psTricks pictures are first compiled
                for item in examList:
                    for path in item.imgPath: 
                        fullPath = os.getcwd() + os.sep + os.path.normpath(str(path[1]))
                        imgFile = os.path.split(fullPath)[1]
                        name, imgExt = os.path.splitext(imgFile)
                        if imgExt.lower()==".tex":
                            self.compileTex(fullPath)
                            with open(fullPath.replace(".tex",".png"), "rb") as imageFile:
                                base64_bytes = base64.b64encode(imageFile.read())
                        elif imgExt.lower()==".png":
                            with open(fullPath, "rb") as imageFile:
                                base64_bytes = base64.b64encode(imageFile.read())
                        else:
                            print("Unsupported image type")
                        item.imgData[name] = base64_bytes.decode('UTF-8') # using UTF-8 encoding
            file.write(prettify(constructXml(examList,self.readEntries())))
            file.close()
            self.statusBar['text'] = "XML saved"
            self.statusBar['foreground'] = "green"
        
    def create_widgets(self):
        # 1st row
        Button(self,
               text ='Open tex',
               command = lambda options="tex": self.openFile(options)
               ).grid(row=0,column=0,sticky="w")
        Button(self,
               text ='Open config',
               command = lambda options="cfg": self.openFile(options)
               ).grid(row=0,column=1,sticky="w")
        # 2nd row
        Label(self,text="Category description:").grid(row=1,column=0,sticky="w")
        Entry(self, width=70, textvariable=self.categoryText).grid(row=1,column=1)
        # 3rd row
        Label(self,text="Category path:").grid(row=2,column=0,sticky="w")
        Entry(self, width=70, textvariable=self.categoryPath).grid(row=2,column=1)
        # 4th row
        Label(self,text="Question name:").grid(row=3,column=0,sticky="w")
        Entry(self, width=70, textvariable=self.questionName).grid(row=3,column=1)
        # 5th row
        Label(self,text="Feedback text (correct answer):").grid(row=4,column=0,sticky="w")
        Entry(self, width=70, textvariable=self.correctFeedbackText).grid(row=4,column=1)
        # 6th row
        Label(self,text="Feedback text (incorrect answer):").grid(row=5,column=0,sticky="w")
        Entry(self, width=70, textvariable=self.incorrectFeedbackText).grid(row=5,column=1)
        # 7th row
        Label(self,text="Feedback text (part. corr. answer):").grid(row=6,column=0,sticky="w")
        Entry(self, width=70, textvariable=self.partiallyCorrectFeedbackText).grid(row=6,column=1)
        # 8th row
        Label(self,text="Max points for answer:").grid(row=7,column=0,sticky="w")
        Entry(self, width=70, textvariable=self.maxPointsForAnswer).grid(row=7,column=1)
        # 9th row
        Checkbutton(self, text="Shuffle answers", var=self.shuffleAnswers).grid(row=8,column=0, sticky=W)
        # 10th row
        Checkbutton(self, text="Include images in xml", var=self.parseImg).grid(row=9,column=0, sticky=W)
        Radiobutton(self, text = "Build using MiKTeX", variable = self.useDocker, value = FALSE).grid(row=10,column=0, sticky=W)
        Radiobutton(self, text = "Build using docker", variable = self.useDocker, value = TRUE).grid(row=10,column=1, sticky=W)
        # 11th row
        Label(self,text="Full path to MiKTeX binaries:").grid(row=11,column=0,sticky="w")
        Entry(self, width=70, textvariable=self.pathToMikTex).grid(row=11,column=1)
        # 12th row
        Button(self,
               text ='Run and Save XML',
               command = lambda options="xml": self.saveFile(options)
               ).grid(row=12,column=0,sticky="w")
        Button(self,
               text ='Save config',
               command = lambda options="cfg": self.saveFile(options)
               ).grid(row=12,column=1,sticky="w",pady=10)

        
        # Bottom
        self.statusBar = Label(self,text="",bd=1,relief=SUNKEN,width=87,anchor=W)
        self.statusBar.grid(row=13,columnspan=2,sticky=W+E)

##########################################################################################

# Replace orig serialize with custom one (needed for CDATA support)
ElementTree._original_serialize_xml = ElementTree._serialize_xml
ElementTree._serialize_xml = ElementTree._serialize['xml'] = _serialize_xml


root = Tk()
root.title('Tex2Moodle')

app = Application(root)

root.mainloop()        
        
